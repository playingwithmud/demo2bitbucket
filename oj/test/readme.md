# oj系统部分
## i.tomcat python 环境安装
* 系统必须有java和python环境

1. 修改tomcat web.xml配置文件
如：C:\Program Files\Apache Software Foundation\Tomcat 7.0\conf\web.xml

去掉web.xml中以下部分cgi的注释：
```
<servlet>
 <servlet-name>cgi</servlet-name>
 <servlet-class>org.apache.catalina.servlets.CGIServlet</servlet-class>
 <init-param>
 <param-name>debug</param-name>
 <param-value>0</param-value>
 </init-param>
 <init-param>
 <param-name>cgiPathPrefix</param-name>
 <param-value>WEB-INF/cgi</param-value>
 </init-param>
 <load-on-startup>5</load-on-startup>
</servlet>

<servlet-mapping>
 <servlet-name>cgi</servlet-name>
 <url-pattern>/cgi-bin/*</url-pattern>
</servlet-mapping>
```
2. 添加servlet "passShellEnvironment"
```
<init-param>
          <param-name>passShellEnvironment</param-name>
          <param-value>true</param-value>
</init-param> 
```
3. 添加servlet executable属性
```
<init-param>
          <param-name>executable</param-name>
          <param-value>C:\Python36\python.exe</param-value>
 </init-param> 
```

### 最终应该有如下结果
```
<servlet>
        <servlet-name>cgi</servlet-name>
        <servlet-class>org.apache.catalina.servlets.CGIServlet</servlet-class>
        <init-param>
          <param-name>debug</param-name>
          <param-value>0</param-value>
        </init-param>
        <init-param>
          <param-name>cgiPathPrefix</param-name>
          <param-value>WEB-INF/cgi</param-value>
        </init-param>
        <init-param>
          <param-name>executable</param-name>
          <param-value>C:\Python36\python.exe</param-value>
        </init-param>
        <init-param>
          <param-name>passShellEnvironment</param-name>
          <param-value>true</param-value>
        </init-param>
         <load-on-startup>5</load-on-startup>
    </servlet> 

```
4. 修改tomcat中content.xml配置文件,将privileged属性添加进去
```
 <Context privileged="true">
...
</Context>
```

# 到此配置完成，下面是运行部分
## ii.项目运行
* python文件必须要在项目的WEB-INF/cgi根目录下才能运行
* 访问方式：
http://localhost:8080/项目名称/cgi-bin/.../python文件
1. 将test项目复制至<TOMCAT_HOME>\webapps目录下，如C:\Program Files\Apache Software Foundation\Tomcat 6.0\webapps
2. 打开tomcat(运行tomcat主目录下，bin目录下的tomcat.exe)
3. 在浏览器中打开网址 http://localhost:8080/test/test.jsp ，即项目主页
4. 点击按钮，结果会出现在C:\Program Files\Apache Software Foundation\Tomcat 6.0\webapps\test\WEB-INF\cgi\oj_test\result.txt中
运行py脚本部分由ajax实现
![avatar](p1.png)

## iii.模型部分
* 模型运行部分主要依赖于run1和run2
* run1.py复制模型和测试代码文件
* run2.py生成模型并进行测试
### run1.py
![avatar](p2.png)
run1中 fo = open()中分别是模型和测试代码地址，必须写为绝对路径。

### test_code.py
test_code必须遵从以下格式，因为需要加入写入结果到result，需引入第三个脚本。
```python
from Model1 import model
class test_code():
    def __init__(self):
        self.result = 0
    def get_result(self):
        test = model()
        test_result = test.get_result()
        self.result = test_result
        return self.result
```
### run2.py
```python
from test_code import test_code
if __name__ == "__main__":
    test = test_code()
    test_result = test.get_result()
    print(test_result)
    fo = open("result.txt", "w+")
    fo.write(str(test_result))
    fo.close()
```

### iii.项目结构
![avatar](p3.png)
* upl中暂存模型和测试代码
* WEB-INF下的cgi文件夹下提供运行环境
* jquery-2.1.1.js提供test.jsp对jquery支持
* test.jsp是测试项目入口