## Deployment test: freelancer
+ This test is performed on Ganache
+ For simplification, all functions are sychronous
+ Tasks:
    + Test 1: View all freelancers' address, contract address and balance
    + Test 2: Accept a request
        + freelancer's address
        + freelancer's contract address
        + client's address
        + client's contract address
        + request's index
    + Test 3: View the number of accepted requests
    + Test 4: View all accepted requests
    + Test 5: Search for a request by name
