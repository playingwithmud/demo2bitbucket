## Deployment test: client
+ This test is performed on Ganache
+ 2 clients will be tested, one with 3 requests, another with 5 requests
+ For simplification, all functions are sychronous
+ Tasks:
    + Test 0: post multiple requests for testing
        + In Client_submit.sol the relevant functions are: client1_test_posts_3requests() and client2_test_posts_5requests
    + Test 1: View all clients' address as well as contracts address and their balance
    + Test 2: Post a request
        + Input : client's address, contract's address, request's information
        + A new request will be posted, and can be viewed via other functions
        + When posting a request, the record money will be deposited in this contract
    + Test 3: View the number of requests
        + Input : Client's address, contract's address
        + Then output number of requests
    + Test 4: Find a request's information by index
        + Input : Client's address, contract's address, the request's index
        + Then output all this model's information (visible and invisible)
    + Test 5: View all requests for one client
        + Input : Client's address, contract's address
    + Test 6: View all requests for all clients
    + Test 7: Update request when freelancer submits a model
        + Input: Client's address, contract's address, request's index, model's address, model's test result, freelancer's address
        + Note that only when this model has the best test result and this result is better than threshold will the information be updated
    + Test 8: Close request and perform transaction
