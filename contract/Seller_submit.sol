pragma solidity ^0.4.18;


// COMP9900 Demo 1 Seller
//
// Author: Yunqiu Xu
// 
// [1] Post a model
// [2] Update a model
// [3] Get the number of posted models
// [4] View a model by its index
// [5] Test functions


contract Seller {
    
    address private owner; // the owner of this contract owner = msg.sender = seller
    
    // event to view information from web3js
    event ModelPosted(address _sellerAddr, string _modelName, string _modelDescriptionAddr, string _modelAddr, uint _modelPrice, uint256 _modelTime, uint _modelSellerIndex);
    event ModelUpdated(address _sellerAddr, string _modelName, string _modelDescriptionAddr, string _modelAddr, uint _modelPrice, uint256 _modelTime, uint _modelSellerIndex);

    // Modifier, owner -> msg.sender -> seller
    modifier isOwner {
        require(owner == msg.sender);
        _;
    }
    
    function Seller() public payable {
        owner = msg.sender;
    }

    // A model's structure
    struct Model {
        string modelName;             // the name of model
        string modelDescriptionAddr; // the address of model description
        string modelAddr;            // the address of model, the user can get model code via this address
        uint modelPrice;              // the price of model (will also be stored in database as well)
        uint256 modelTime;            // the time this model be posted / updated (in second)
    }
    
    // Collect the models this seller posted
    Model[] public postedModels;
    
    // [1] Seller posts a new model
    function postModel(string _modelName, string _modelDescriptionAddr, string _modelAddr, uint _modelPrice, uint256 _modelTime) public isOwner() {
        postedModels.push(Model(_modelName, _modelDescriptionAddr, _modelAddr, _modelPrice, _modelTime));
        uint modelSellerIndex = getModelCount() - 1;
        ModelPosted(owner, _modelName, _modelDescriptionAddr, _modelAddr, _modelPrice, _modelTime, modelSellerIndex);
    }

    // [2] Seller updates the model (only the price)
    function updateModel(uint _modelSellerIndex, uint _newPrice, uint256 _modelTime) public isOwner() returns(bool) {
        if (_modelSellerIndex < postedModels.length) {
            postedModels[_modelSellerIndex].modelPrice = _newPrice;
            postedModels[_modelSellerIndex].modelTime = _modelTime;
            ModelUpdated(owner, postedModels[_modelSellerIndex].modelName, postedModels[_modelSellerIndex].modelDescriptionAddr, postedModels[_modelSellerIndex].modelAddr, postedModels[_modelSellerIndex].modelPrice, postedModels[_modelSellerIndex].modelTime, _modelSellerIndex);
            return true;
        }
        return false;
    }

    // [3] Get model count
    function getModelCount() view public returns(uint) {
        return postedModels.length;
    }

    // [4] Get a model directly by their index
    // input : index in postedModels
    // output : seller's name / model's name / description's addr / model's addr / price / time / index
    function getModelByIndex(uint _modelSellerIndex) view public returns(address, string, string, string, uint, uint256, uint) {
         Model storage currentModel = postedModels[_modelSellerIndex]; 
         return (owner, currentModel.modelName, currentModel.modelDescriptionAddr, currentModel.modelAddr, currentModel.modelPrice, currentModel.modelTime, _modelSellerIndex);
    }

    // Call back
    function () public payable {}

    // [Seller 1 only] [6] This function will post 5 models
    function seller1_test_posts_5models(uint256 _modelTime) public {
        postModel("Linear Regression", "description", "modelAddress_1-1", 0.000000001 ether, _modelTime);
        postModel("Logistic Regression", "description", "modelAddress_1-2", 0.000000001 ether, _modelTime);
        postModel("Neural Network", "description", "modelAddress_1-3", 0.000000001 ether, _modelTime);
        postModel("Decision Tree", "description", "modelAddress_1-4", 0.000000001 ether, _modelTime);
        postModel("SVM", "description", "modelAddress_1-5", 0.000000001 ether, _modelTime);
    }

    // [Seller 2 only] [6] This function will post 20 models
    function seller2_test_posts_20models(uint256 _modelTime) public {
        postModel("Human-level control through deep reinforcement learning", "description", "modelAddress_2-1", 0.000000001 ether, _modelTime);
        postModel("Mastering the game of Go with deep neural networks and tree search", "description", "modelAddress_2-2", 0.000000001 ether, _modelTime);
        postModel("End-to-end training of deep visuomotor policies", "description", "modelAddress_2-3", 0.000000001 ether, _modelTime);
        postModel("Learning hand-eye coordination for robotic grasping with large-scale data collection", "description", "modelAddress_2-4", 0.000000001 ether, _modelTime);
        postModel("Towards adapting deep visuomotor representations from simulated to real environments", "description", "modelAddress_2-5", 0.000000001 ether, _modelTime);
        postModel("Target-driven visual navigation in indoor scenes using deep reinforcement learning", "description", "modelAddress_2-6", 0.000000001 ether, _modelTime);
        postModel("Asynchronous methods for deep reinforcement learning", "description", "modelAddress_2-7", 0.000000001 ether, _modelTime);
        postModel("Prioritized experience replay", "description", "modelAddress_2-8", 0.000000001 ether, _modelTime);
        postModel("Hindsight experience replay", "description", "modelAddress_2-9", 0.000000001 ether, _modelTime);
        postModel("Robust imitation of diverse behaviors", "description", "modelAddress_2-10", 0.000000001 ether, _modelTime);
        postModel("Generative adversarial imitation learning", "description", "modelAddress_2-11", 0.000000001 ether, _modelTime);
        postModel("A Deep Hierarchical Approach to Lifelong Learning in Minecraft", "description", "modelAddress_2-12", 0.000000001 ether, _modelTime);
        postModel("The Option-Critic Architecture", "description", "modelAddress_2-13", 0.000000001 ether, _modelTime);
        postModel("Feudal networks for hierarchical reinforcement learning", "description", "modelAddress_2-14", 0.000000001 ether, _modelTime);
        postModel("Curriculum learning", "description", "modelAddress_2-15", 0.000000001 ether, _modelTime);
        postModel("Teacher-Student Curriculum Learning", "description", "modelAddress_2-16", 0.000000001 ether, _modelTime);
        postModel("Model-agnostic meta-learning for fast adaptation of deep networks", "description", "modelAddress_2-17", 0.000000001 ether, _modelTime);
        postModel("One-Shot Imitation from Observing Humans via Domain-Adaptive Meta-Learning", "description", "modelAddress_2-18", 0.000000001 ether, _modelTime);
        postModel("Rainbow: Combining Improvements in Deep Reinforcement Learning", "description", "modelAddress_2-19", 0.000000001 ether, _modelTime);
        postModel("Learning from demonstrations for real world reinforcement learning", "description", "modelAddress_2-20", 0.000000001 ether, _modelTime);
    }
    
}
