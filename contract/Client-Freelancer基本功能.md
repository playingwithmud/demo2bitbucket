# Contract : Client

+ postRequest() : post a new request
+ updateRequest() : 
    + Note that this is rather different from what in Seller.sol. 
    + This function is called when a freelancer submit a model
    + This function will compare the test result with current best result and threshold, then update the **model address, test result and freelancer address** for this request
+ getRequestCount() : get the number of requests for this client
+ getRequestByIndex() : get one request by its index (within this client)
    + Note that I need to split this function as 2 parts to aviod "too deep stack exception"
+ closeRequest() : 
    + set request_end = True
    + perform transaction
    + visualize result to client (**in html**) 
    
# Contract : Freelancer
+ acceptRequest() : accept a request
+ getRequestCount() : get the number of requests he accepted
+ getRequestByIndex() : get the information of a request by its index
+ checkRepeat() : the same request should not be acccepted twice
+ different from "model" in buyer-seller, here the "prinmary key" to identify a request should be client's address as well as the index within client contract
